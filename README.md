# SonicBrutus

Brute Force to SonicWall devices.

## Dependency

Install casperjs (http://docs.casperjs.org/en/latest/installation.html)[http://docs.casperjs.org/en/latest/installation.html] and phantomjs (https://tecadmin.net/install-phantomjs-on-ubuntu/)[https://tecadmin.net/install-phantomjs-on-ubuntu/].

## Files

- **oneUserCheck.js** : One target and one user password check to SonicWall login.
- **oneTarget.sh** : One target with multiple try user password.
- **multiTarget.sh** : Multiple target with multiple try user password.

## Usage 

- Try one user and one password on one SonicWall:
```javascript
casperjs run.js --ignore-ssl-errors=true --ssl-protocol=any --user='admin' --pass='password' --url="https://0.0.0.0:444"
```
- Try one target with multiple user password dictionaries.
```bash
bash oneTarget.sh "https://0.0.0.0:443/auth.html" "users.list" "pass.txt"
```
- Try multiple targets with multiple user password dictionaries.
```bash
bash multiTarget.sh "fileTargets.txt" "fileUsername.txt" "filePasswords.txt"
```
