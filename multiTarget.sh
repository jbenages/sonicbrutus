#!/bin/bash

# USAGE: bash multiTarget.sh "fileTargets.txt" "fileUsername.txt" "filePasswords.txt"

FILETARGETS=$1
FILEUSERS=$2
FILEPASS=$3
FOLDERLOGS="."

while read -r TARGET ;do
	echo $TARGET 
	curl -s -k -L $TARGET/ > /dev/null
	if [ $? != 1 ];then
	    IP=$(echo "${TARGET}" | awk -F/ '{print $3}' | sed 's/:.*//')
	    PORT=$(echo $TARGET | sed -e 's,^.*:,:,g' -e 's,.*:\([0-9]*\).*,\1,g' -e 's,[^0-9],,g')
	    mkdir -p $FOLDERLOGS
	    bash oneTarget.sh "$TARGET" "$FILEUSERS" "$FILEPASS" | tee "$FOLDERLOGS/sonicbrutus-$IP-"$(date +"%Y%m%d%H%M%S")".log"
	fi
done < "$FILETARGETS"

