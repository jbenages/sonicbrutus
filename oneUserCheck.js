/*
 * Usage casperjs run.js --ignore-ssl-errors=true --ssl-protocol=any --user='admin' --pass='password' --url="https://0.0.0.0:444"
 */

var casper = require('casper').create({
//    verbose: true,
//    logLevel: "debug",
    pageSettings: {
      javascriptEnabled: true,
      loadImages: true,
      loadPlugins: true
    }
});

phantom.cookiesEnabled = true;
casper.userAgent('Mozilla/4.0 (compatable; MSIE 6.0; Windows NT 5.1)');

casper.start(casper.cli.get('url') + "/auth.html");

casper.on('url.changed',function(url) {
    if( url.indexOf('dynLoginLockout.html') != -1 ){
        this.echo("\e[1m31m[!]BLOCKED!\e[0m");
    }
});

casper.waitForSelector('frame[id="authFrm"]', function() {
    this.page.switchToChildFrame("authFrm");
    this.fillSelectors('form[name="standardPass"]', {
        'input[name = userName ]' : casper.cli.get('user'),
        'input[name = pwd ]' : casper.cli.get('pass')
    }, true);
    this.click("input[type='submit']");
});

casper.waitForSelector('frame[id="authFrm"]', function(){
    this.page.switchToChildFrame("authFrm");
    var text = this.evaluate(function(){
        return document.querySelector("div[id='error_text']").textContent;
    });
    this.page.switchToParentFrame()
    if ( text == "" ){
        if ( this.getCurrentUrl() != casper.cli.get('url') ){
            this.echo("\e[1m32mDONE! \e[0m") ;
	}else{
           this.echo("\e[1m33mMay be done\e[0m");
        }
    }else{
        this.echo(text);
    }
});

casper.run();
